<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Already have an account?' => 'Вже зареєстровані?',
    'I forgot my password' => 'Забув свій пароль',
    'Join now!' => 'Приєднатися зараз!',
    'Need new verification email?' => 'Потрібен новий email верифікації?',
    'OR' => 'АБО',
    'Please choose your new password:' => 'Будь ласка, оберіть новий пароль:',
    'Please fill out the following fields to signup:' => 'Будь ласка, заповніть наступні поля для реєстрації:',
    'Please fill out your email. A link to reset password will be sent there.' => 'Будь ласка, вкажіть вашу електронну адресу. Посилання на відновлення паролю буде відправлено на неї.',
    'Please fill out your email. A verification email will be sent there.' => 'Будь ласка, вкажіть вашу електронну адресу. Лист верифікації буде відправлено на неї.',
    'Register a new account' => 'Зареєструвати новий аккаунт',
    'Registration' => 'Реєстрація',
    'Request password reset' => 'Запросити відновлення паролю',
    'Resend' => 'Повторно відправити',
    'Resend verification email' => 'Повторно відправити лист верифікації',
    'Reset password' => 'Відновити пароль',
    'Save' => 'Зберегти',
    'Send' => 'Надіслати',
    'Sign In' => 'Авторизуватися',
    'Sign in' => 'Авторизуватися',
    'Sign in to start your session' => 'Авторизуватися, щоб розпочати вашу сессію',
    'Sign in using Facebook' => 'Авторизуватися через Facebook',
    'Sign in using Google+' => 'Авторизуватися через Google+',
    'Sign in!' => 'Авторизуватися!',
];
