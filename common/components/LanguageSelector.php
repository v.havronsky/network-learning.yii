<?php

namespace common\components;

use Yii;
use yii\base\Widget;

class LanguageSelector extends Widget
{
    public $selected;
    public $languages;

    public function init()
    {
        parent::init();

        if ($this->selected === null) {
            $this->selected = Yii::$app->language;
        }
    }

    public function run()
    {
        return $this->render('language-selector', [
            'selected' => $this->selected,
        ]);
    }
}
