<?php
/* @var $selected string selected language */

use yii\bootstrap\Dropdown;
use yii\helpers\Url;

$languages = [
    'en-US' => '<span class="flag-icon flag-icon-us"></span>&nbsp;' . Yii::t('common/nav', 'English'),
    'uk-UA' => '<span class="flag-icon flag-icon-ua"></span>&nbsp;' . Yii::t('common/nav', 'Ukrainian')
];

?>

<li class="dropdown hidden-xs">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><?= $languages[$selected] ?> <b class="caret"></b></a>
    <?= Dropdown::widget([
        'encodeLabels' => false,
        'items' => [
            ['label' => $languages['en-US'], 'url' => Url::current(['language' => 'en-US'])],
            ['label' => $languages['uk-UA'], 'url' => Url::current(['language' => 'uk-UA'])],
        ],
    ]); ?>
</li>