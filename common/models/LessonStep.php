<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lesson_steps".
 *
 * @property int $id
 * @property int $id_lesson
 * @property string $name
 * @property int $pos
 * @property int $active
 * @property string $content
 * @property string $icon
 * @property string $creation_date
 * @property string $timestamp
 */
class LessonStep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_steps';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_lesson', 'name', 'content'], 'required'],
            [['id_lesson', 'pos', 'active'], 'integer'],
            [['content'], 'string'],
//            [['creation_date', 'timestamp'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['icon'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_lesson' => 'Id Lesson',
            'name' => 'Name',
            'pos' => 'Pos',
            'active' => 'Active',
            'content' => 'Content',
            'icon' => 'Icon',
            'creation_date' => 'Creation Date',
            'timestamp' => 'Timestamp',
        ];
    }
}
