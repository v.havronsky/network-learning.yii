<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lessons".
 *
 * @property int $id
 * @property int $id_course
 * @property string $name
 * @property int $pos
 * @property int $active
 * @property string $short_desc
 * @property string $creation_date
 * @property string $data
 * @property string $timestamp
 */
class Lesson extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lessons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_course', 'pos', 'active'], 'integer'],
            [['id_course', 'name'], 'required'],
            [['short_desc', 'data'], 'string'],
//            [['creation_date', 'timestamp'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_course' => 'Id Course',
            'name' => 'Name',
            'pos' => 'Pos',
            'active' => 'Active',
            'short_desc' => 'Short Desc',
            'creation_date' => 'Creation Date',
            'data' => 'Data',
            'timestamp' => 'Timestamp',
        ];
    }

    public function getSteps()
    {
        return $this->hasMany(LessonStep::class, ['id_lesson' => 'id'])->orderBy(['pos' => SORT_ASC]);
    }

    public function getTest()
    {
        return $this->hasOne(Test::class, ['id_lesson' => 'id']);
    }

    public function getCourse()
    {
        return $this->hasOne(Course::class, ['id' => 'id_course']);
    }
}
