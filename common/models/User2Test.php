<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user2test".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_test
 * @property string $complete_date
 * @property double $mark
 * @property double $max_mark
 * @property int $questions_count
 * @property int $questions_correct_count
 */
class User2Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user2test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_test', 'complete_date', 'mark', 'max_mark', 'questions_count', 'questions_correct_count'], 'required'],
            [['id_user', 'id_test', 'questions_count', 'questions_correct_count'], 'integer'],
            [['complete_date'], 'safe'],
            [['mark', 'max_mark'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/model/test', 'ID'),
            'id_user' => Yii::t('common/model/test', 'Id User'),
            'id_test' => Yii::t('common/model/test', 'Id Test'),
            'complete_date' => Yii::t('common/model/test', 'Complete Date'),
            'mark' => Yii::t('common/model/test', 'Mark'),
            'max_mark' => Yii::t('common/model/test', 'Max Mark'),
            'questions_count' => Yii::t('common/model/test', 'Questions Count'),
            'questions_correct_count' => Yii::t('common/model/test', 'Questions Correct Count'),
        ];
    }
}
