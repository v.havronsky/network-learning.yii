<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user2course".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_course
 * @property string $sign_date
 * @property string $complete_date
 */
class User2Course extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user2course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_course'], 'required'],
            [['id_user', 'id_course'], 'integer'],
            [['sign_date', 'complete_date'], 'safe'],
            [['id_user', 'id_course'], 'unique', 'targetAttribute' => ['id_user', 'id_course']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_course' => 'Id Course',
            'sign_date' => 'Sign Date',
            'complete_date' => 'Complete Date',
        ];
    }
}
