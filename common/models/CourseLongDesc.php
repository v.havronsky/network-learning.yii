<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "course_long_desc".
 *
 * @property int $id
 * @property int $id_course
 * @property string $content
 * @property string $timestamp
 */
class CourseLongDesc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_long_desc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_course', 'content'], 'required'],
            [['id_course'], 'integer'],
            [['content'], 'string'],
            [['timestamp'], 'safe'],
            [['id_course'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_course' => 'Id Course',
            'content' => 'Content',
            'timestamp' => 'Timestamp',
        ];
    }
}
