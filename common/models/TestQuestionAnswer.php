<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "test_question_answers".
 *
 * @property int $id
 * @property int $id_question
 * @property string $text
 * @property int $pos
 * @property int $is_correct
 * @property string $timestamp
 */
class TestQuestionAnswer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test_question_answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_question', 'text', 'pos'], 'required'],
            [['id_question', 'pos', 'is_correct'], 'integer'],
            [['text'], 'string'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/model/test', 'ID'),
            'id_question' => Yii::t('common/model/test', 'Id Question'),
            'text' => Yii::t('common/model/test', 'Text'),
            'pos' => Yii::t('common/model/test', 'Pos'),
            'is_correct' => Yii::t('common/model/test', 'Is Correct'),
            'timestamp' => Yii::t('common/model/test', 'Timestamp'),
        ];
    }
}
