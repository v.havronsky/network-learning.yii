<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "test_questions".
 *
 * @property int $id
 * @property int $id_test
 * @property string $text
 * @property int $pos
 * @property double $cost
 * @property string $timestamp
 */
class TestQuestion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test_questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_test', 'text', 'pos'], 'required'],
            [['id_test', 'pos'], 'integer'],
            [['text'], 'string'],
            [['cost'], 'number'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/model/test', 'ID'),
            'id_test' => Yii::t('common/model/test', 'Id Test'),
            'text' => Yii::t('common/model/test', 'Text'),
            'pos' => Yii::t('common/model/test', 'Pos'),
            'cost' => Yii::t('common/model/test', 'Cost'),
            'timestamp' => Yii::t('common/model/test', 'Timestamp'),
        ];
    }

    public function getAnswers()
    {
        return $this->hasMany(TestQuestionAnswer::class, ['id_question' => 'id'])->orderBy(['pos' => SORT_ASC]);
    }
}
