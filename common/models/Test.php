<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tests".
 *
 * @property int $id
 * @property int $id_course is final
 * @property int $id_lesson
 * @property string $name
 * @property string $timestamp
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_course', 'id_lesson'], 'integer'],
            [['name'], 'required'],
            [['timestamp'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/model/test', 'ID'),
            'id_course' => Yii::t('common/model/test', 'Id Course'),
            'id_lesson' => Yii::t('common/model/test', 'Id Lesson'),
            'name' => Yii::t('common/model/test', 'Name'),
            'timestamp' => Yii::t('common/model/test', 'Timestamp'),
        ];
    }

    public function getQuestions()
    {
        return $this->hasMany(TestQuestion::class, ['id_test' => 'id'])->orderBy(['pos' => SORT_ASC]);
    }

    public function getLesson()
    {
        return $this->hasOne(Lesson::class, ['id' => 'id_lesson']);
    }

    public function getCourse()
    {
        return $this->hasOne(Course::class, ['id' => 'id_course']);
    }
}
