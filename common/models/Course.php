<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $name
 * @property int $id_user
 * @property string $short_desc
 * @property int $active
 * @property string $logo_img
 * @property string $creation_date
 * @property string $timestamp
 */
class Course extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'id_user'], 'required'],
            [['id_user', 'active'], 'integer'],
            [['short_desc'], 'string'],
            [['creation_date', 'timestamp'], 'safe'],
            [['name', 'logo_img'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/model/course', 'ID'),
            'name' => Yii::t('common/model/course', 'Name'),
            'id_user' => Yii::t('common/model/course', 'Parent ID'),
            'short_desc' => Yii::t('common/model/course', 'Short Desc'),
            'active' => Yii::t('common/model/course', 'Active'),
            'logo_img' => Yii::t('common/model/course', 'Logo Img'),
            'creation_date' => Yii::t('common/model/course', 'Creation Date'),
            'timestamp' => Yii::t('common/model/course', 'Timestamp'),
        ];
    }

    public function getCourseLongDesc()
    {
        return $this->hasOne(CourseLongDesc::className(), ['id_course' => 'id']);
    }
}
