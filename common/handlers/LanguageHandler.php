<?php

namespace common\handlers;

use Yii;
use yii\base\Behavior;
use yii\web\Cookie;

/**
 * Class LanguageHandler
 * @package common\handlers
 */
class LanguageHandler extends Behavior
{
    /**
     * @var string
     */
    public static $defaultLanguage = 'uk-UA';

    /**
     * @var array
     */
    public static $availableLanguages = ['en-US', 'uk-UA'];

    /**
     * Checks if language is in available languages
     *
     * @param string $lang language code
     *
     * @return bool
     */
    public static function isValidLanguage($lang)
    {
        return in_array($lang, self::$availableLanguages);
    }

    /**
     * Main function for language handling
     * Check request to set new language cookie
     * Check cookies to get language
     */
    public static function main()
    {
        // default language
        $language = self::$defaultLanguage;

        // try to get language from cookies
        $cookies = Yii::$app->request->cookies;
        if ($cookies && $lang = $cookies->getValue('language')) {
            if (self::isValidLanguage($lang)) {
                $language = $lang;
            }
        }

        // check request for new language handle
        if ($lang = Yii::$app->request->get('language')) {
            if (self::isValidLanguage($lang)) {
                // add a new cookie to the response to be sent
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'language',
                    'value' => $lang,
                ]));

                $language = $lang;
            }
        }

        Yii::$app->language = $language;
        Yii::$app->formatter->locale = $language;
    }
}
