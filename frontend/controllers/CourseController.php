<?php

namespace frontend\controllers;

use Yii;
use yii\data\Pagination;
use frontend\models\Course;
use frontend\models\CourseSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class CourseController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true, //@todo allow -> false do not work properly
//                        'actions' => ['list', 'sign-up'],
                        'actions' => ['index', 'view'],
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'sign-up' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $dataProvider->getCount(),
        ]);

        return $this->render('index', [
            'courses' => $dataProvider->getModels(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pagination' => $pagination,
        ]);
    }

    public function actionList()
    {
        $searchModel = new CourseSearch();

        // get user courses
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, Yii::$app->user->getId());

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $dataProvider->getCount(),
        ]);

        return $this->render('list', [
            'courses' => $dataProvider->getModels(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pagination' => $pagination,
        ]);
    }

    public function actionSignUp($id)
    {
        if (Course::assignUser(Yii::$app->user->getId(), $id)) {
            Yii::$app->session->setFlash('success', Yii::t('common/flash', 'Successfully subscribed!'));

        } else {
            Yii::$app->session->setFlash('error', Yii::t('common/flash','Some error has occurred!'));
        }

        return $this->redirect(['course/view', 'id' => $id]);
    }

    public function actionView($id)
    {
        $course = $this->findModel($id);

        $course_lessons_list = $this->renderPartial('lessons-list', [
            'lessons' => $course->lessons,
        ]);

        $signed_users = [];
        foreach ($course->users as $user) {
            $signed_users[] = $user->id;
        }
        $is_user_signed = in_array(Yii::$app->user->getId(), $signed_users);

        return $this->render('view', [
            'course' => $course,
            'long_desc_model' => $course->courseLongDesc,
            'course_lessons_list' => $course_lessons_list,
            'is_user_signed' => $is_user_signed,
        ]);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
