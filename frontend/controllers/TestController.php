<?php

namespace frontend\controllers;

use common\models\Test;
use frontend\models\TestForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class TestController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param string $id test id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($id)
    {
        $test = $this->findModel($id);

        $form = new TestForm();
        $form->getTestForm($test);

        if (Yii::$app->request->post() && $form->load(Yii::$app->request->post()) && $form->validate()) {
            if ($results = $form->checkAnswers()) {
                return $this->render('results', [
                    'results' => $results,
                    'test' => $test,
                    'model' => $form,
                ]);
            }
        }

        return $this->render('index', [
            'test' => $test,
            'model' => $form,
        ]);
    }
//    public function actionCheck()
//    {
//        return $this->render('index', [
//            'test' => $test,
//            'model' => $form,
//        ]);
//    }

    protected function findModel($id)
    {
        if (($model = Test::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
