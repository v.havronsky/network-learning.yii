<?php

namespace frontend\controllers;

use backend\models\course\Course;
use common\models\CourseLongDesc;
use frontend\models\AccountUser;
use frontend\models\CourseSearch;
use Yii;
use common\models\User;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * AccountController implements the CRUD actions for User model.
 */
class AccountCourseController extends \backend\controllers\CourseController
{
    protected $user_id;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'update' => ['POST'],
//                ],
//            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $this->user_id = Yii::$app->user->getId();

        return true;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = $this->findUser($this->getUserId());

        $searchModel = new CourseSearch();

        // get user own courses
        $dataProvider = $searchModel->searchOwn(Yii::$app->request->queryParams, $this->getUserId());

        return $this->render('index', [
            'model' => $model,
            'courses' => $dataProvider->getModels(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param bool $random
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();
        $long_desc_model = new CourseLongDesc();


        if ($this->handlePostSave($model)) {
//            $model = $this->findModel($model->id);
//            if (!$long_desc_model = $model->courseLongDesc) {
//                $long_desc_model = new CourseLongDesc();
//            }
//
//            return $this->render('update', [
//                'model' => $model,
//                'long_desc_model' => $long_desc_model,
//            ]);
            Yii::$app->session->setFlash('success', Yii::t('common/flash', 'Successfully created!'));

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'long_desc_model' => $long_desc_model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!$long_desc_model = $model->courseLongDesc) {
            $long_desc_model = new CourseLongDesc();
        }

        if ($this->handlePostSave($model)) {
            Yii::$app->session->setFlash('success', Yii::t('common/flash', 'Successfully updated!'));

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'long_desc_model' => $long_desc_model,
        ]);
    }

    protected function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUser($id)
    {
        if (($model = AccountUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//    protected function findModel($id)
//    {
//        if (($model = AccountUser::findOne($id)) !== null) {
//            return $model;
//        } else {
//            throw new NotFoundHttpException('The requested page does not exist.');
//        }
//    }
}
