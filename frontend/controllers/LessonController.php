<?php

namespace frontend\controllers;

use common\models\Lesson;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class LessonController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($id)
    {
        $lesson = $this->findModel($id);

//        $course_lessons_list = $this->renderPartial('lessons-list', [
//            'lessons' => $lesson->lessons,
//        ]);

        return $this->render('index', [
            'lesson' => $lesson,
            'lesson_steps' => $lesson->steps,
//            'long_desc_model' => $course->courseLongDesc,
//            'course_lessons_list' => $course_lessons_list,
        ]);
        return $this->render('index');
    }

    /**
     * Finds the Lesson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lesson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lesson::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
