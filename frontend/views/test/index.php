<?php
/* @var $this yii\web\View */
/* @var $test common\models\Test */
/* @var $model frontend\models\TestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $test->name;

if ($test->lesson) {
    $this->params['breadcrumbs'][] = ['label' => $test->lesson->course->name, 'url' => ['/course/view', 'id' => $test->lesson->course->id]];
    $this->params['breadcrumbs'][] = ['label' => $test->lesson->name, 'url' => ['/lesson', 'id' => $test->lesson->id]];
} else if ($test->course) {
    $this->params['breadcrumbs'][] = ['label' => $test->course->name, 'url' => ['/course/view', 'id' => $test->course->id]];
}

$this->params['breadcrumbs'][] = $test->name;

?>

<h1><?= $test->name ?></h1>

<div class="row">
    <div class="col-xs-12">

        <article class="box box-warning">
            <div class="box-body test-form">
<!--                --><?php //$form = ActiveForm::begin(['action' => ['/test/check']]); ?>
                <?php $form = ActiveForm::begin(); ?>
                <div class="box-body table-responsive">
                    <?php foreach ($model->questionsSet as $id => $question): ?>
<!--                        ['label' => Yii::t('app', 'I understand and agree to the <a href="#termsofuse">Terms of Use</a>.')])->label(false)-->
                        <?php echo $form->field($model, "userAnswers[{$id}]")
                            ->label($question['label'])
                            ->{$question['type']}($question['answers']); ?>

                    <?endforeach;?>
                </div>
                <div class="box-footer">
                    <?= Html::submitButton(Yii::t('frontend/test', 'Send'), ['class' => 'btn btn-success btn-flat']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </article>

    </div>
</div>
