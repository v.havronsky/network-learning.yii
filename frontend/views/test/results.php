<?php
/* @var $this yii\web\View */
/* @var $results common\models\User2Test */
/* @var $test common\models\Test */
/* @var $model frontend\models\TestForm */

use yii\helpers\Html;
use yii\widgets\DetailView;
use practically\chartjs\Chart;
//use dosamigos\chartjs\ChartJs;

$formatter = \Yii::$app->formatter;

$this->title = Yii::t('frontend/test', 'Your results | {0}', $test->name);

if ($test->lesson) {
    $this->params['breadcrumbs'][] = ['label' => $test->lesson->course->name, 'url' => ['/course/view', 'id' => $test->lesson->course->id]];
    $this->params['breadcrumbs'][] = ['label' => $test->lesson->name, 'url' => ['/lesson', 'id' => $test->lesson->id]];
} else if ($test->course) {
    $this->params['breadcrumbs'][] = ['label' => $test->course->name, 'url' => ['/course/view', 'id' => $test->course->id]];
}

?>

<h1><?= Yii::t('frontend/test', 'Your results | {0}', $test->name) ?></h1>

<div class="row">
    <div class="col-xs-12">

        <article class="box box-warning">
            <div class="box-body">
<!--                <div class="chart-container" style="max-height:200px; height: 200px;">-->
<!--                <div class="chart-container" style="">-->
<!--                    --><?//= ChartJs::widget([
//                        'type' => 'doughnut',
//                        'options' => [
////                            'id' => 'cstat',
////                            'width' => 250,
////                            'height' => 250,
//                            'responsive' => false,
////                            'maintainAspectRatio' => true,
////                            'responsive' => true,
//                            'maintainAspectRatio' => false,
//                            'showScale' => false,
//                        ],
////                        'clientOptions' => [
////                            'responsive' => false,
////                            'legend' => ['display' => false],
////                            'cutoutPercentage' => 45,
////                            'animation' => [
////                                'animateRotate' => false,
////                                'animateScale' => true,
////                                'duration' => 369,
////                            ],
////                        ],
//                        'data' => [
//                            'labels' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
//                            'datasets' => [
//                                [
//                                    'label' => 'level',
//                                    'data' => [0, 1, 2, 3, 4, 6, 5, 12 , 17, 50, 100],
//                                ]
//                            ],
//                            'Your score' => $results->mark,
//                            'Maximal score' => $results->max_mark,
//                        ],
//                    ]) ?>
<!--                </div>-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="box-header">
                            <?= Yii::t('frontend/test', 'Thank you for completing this test! Your results can be find below:') ?>
                        </div>
                        <div class="box-body table-responsive">
                            <?= DetailView::widget([
                                'model' => $results,
                                'attributes' => [
                                    'complete_date:datetime',
                                    'mark',
                                    'max_mark',
                                    'questions_count',
                                    'questions_correct_count',
                                ],
                            ]) ?>
                        </div>
                        <div class="text-center">
                            <?php if ($model->otherUsersAvgMark): ?>
                                <?php if ($results->mark >= $model->otherUsersAvgMark): ?>
                                    <h3><?= Yii::t('frontend/test', 'Your score is by {0} better than other user\'s!', $formatter->asPercent($results->mark / $model->otherUsersAvgMark - 1, 2)) ?></h3>
                                <? else: ?>
<!--                                    <h3>--><?//= Yii::t('frontend/test', 'Your score is by {0} worse than other user\'s!',    $formatter->asPercent($results->mark / $model->otherUsersAvgMark - 1, 2) * -1) ?><!--</h3>-->
                                <? endif; ?>
                            <? endif; ?>
                            <?= Html::a('<i class="fa fa-share-alt"></i> ' . Yii::t('frontend/test', 'Share results!'), '#', ['class' => 'btn btn-primary btn-xs create_button p-3 mt-3 font-bigger']) ?>
                        </div>
                    </div>
<!--                    <div class="col-md-6">-->
<!--                        --><?//= Chart::widget([
//                            'type' => Chart::TYPE_BAR,
//                            'datasets' => [
//                                [
//                                    'data' => [
//                                        'Your score' => $results->mark,
//                                        'Maximal score' => $results->max_mark,
//                                    ],
//                                ]
//                            ],
//                            'options' => [
//                                'class' => 'chart',
//                                'data-attribute' => 'my-value'
//                            ],
//                            'clientOptions' => [
//                                'title' => [
//                                    'display' => true,
//                                    'text' => Yii::t('frontend/test', 'My results score'),
//                                ],
//                                'legend' => ['display' => false],
//                                'scales' => [
//                                    'yAxes' => [
//                                        [
//                                            'ticks' => [
//                                                'min' => 0,
//                                                'stepSize' => floor($results->max_mark / $results->questions_count),
//                                                'max' => floor($results->max_mark * 1.1),
//                                            ]
//                                        ]
//                                    ]
//                                ],
//                            ],
//                        ]); ?>
<!--                    </div>-->

                    <div class="col-md-6">
                        <?= Chart::widget([
//                            'type' => Chart::TYPE_BAR,
                            'type' => 'horizontalBar',
                            'datasets' => [
                                [
                                    'data' => [
                                        Yii::t('frontend/test','Your score') => $results->mark,
                                        Yii::t('frontend/test','Other users average score') => $model->otherUsersAvgMark,
                                    ],
                                ]
                            ],
                            'options' => [
                                'class' => 'chart',
                                'data-attribute' => 'my-value'
                            ],
                            'clientOptions' => [
                                'title' => [
                                    'display' => true,
                                    'text' => Yii::t('frontend/test', 'My results score compared to other users average score'),
                                ],
//                                'legend' => [
//                                    'labels' => [
//                                        'Your score',
//                                        'Other users average score'
//                                    ]
//                                ],
                                'legend' => ['display' => false],
                                'scales' => [
                                    'xAxes' => [
                                        [
                                            'ticks' => [
                                                'min' => 0,
                                                'stepSize' => floor($results->max_mark / $results->questions_count),
                                                'max' => floor($results->max_mark),
//                                                'max' => floor($results->max_mark * 1.1),
                                            ],
                                            'scaleLabel' => [
                                                'display' => true,
                                                'labelString' => Yii::t('frontend/test', 'Points'),
                                            ],
                                        ]
                                    ],
//                                    'yAxes' => [
//                                        [
//                                            'ticks' => [
//                                                'display' => false
//                                            ]
//                                        ]
//                                    ]
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </article>

    </div>
</div>