<?php

use yii\db\Query;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('frontend/account', 'My profile | {0}', $model->username);
$this->params['breadcrumbs'][] = Yii::t('frontend/account', 'My profile');



$subs_courses_count = (new Query())
    ->from('user2course')
    ->where('id_user=:id_user')
    ->addParams([
        ':id_user' => Yii::$app->user->id,
    ])
    ->count('id');

$completed_courses_count = (new Query())
    ->from('user2course')
    ->where('id_user=:id_user and complete_date IS NOT NULL')
    ->addParams([
        ':id_user' => Yii::$app->user->id,
    ])
    ->count('id');

$created_courses_count = (new Query())
    ->from('courses')
    ->where('id_user=:id_user')
    ->addParams([
        ':id_user' => Yii::$app->user->id,
    ])
    ->count('id');

// overall score
$connection = Yii::$app->getDb();
$command = $connection->createCommand("SELECT SUM(`mark`) as 'gained_score', SUM(`max_mark`) as 'max_score' FROM `user2test` WHERE id_user = :id_user ", [':id_user' => Yii::$app->user->id]);
$overall_score = $command->queryOne(PDO::FETCH_ASSOC);

?>

<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="user-view box box-primary" style="padding-bottom: 90px">
            <div class="box-header">
                <?= Html::a(
                    '<i class="fa fa-pencil"></i> ' . Yii::t('frontend/account', 'Update'),
                    ['update'],
                    [
                        'class' => 'btn btn-primary btn-flat',
                        'data' => [
                            'method' => 'post',
                        ],
                    ]) ?>
            </div>
            <div class="box-body table-responsive no-padding">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'username',
                        'email',
                        'first_name',
                        'last_name',
                        'birthday',
                        'created_at:datetime',
                        'avatar',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">

        <?php echo \insolita\wgadminlte\LteInfoBox::widget([
            'bgIconColor' => \insolita\wgadminlte\LteConst::COLOR_OLIVE,
            'bgColor' => '',
            'number' => $subs_courses_count,
            'text' => Yii::t('frontend/account/stats', 'Number of subscribed courses'),
            'icon' => 'fa fa-star-o',
        ]) ?>
        <?php echo \insolita\wgadminlte\LteInfoBox::widget([
            'bgIconColor' => \insolita\wgadminlte\LteConst::COLOR_PURPLE,
            'bgColor' => '',
            'number' => $completed_courses_count,
            'text' => Yii::t('frontend/account/stats', 'Number of completed courses'),
            'icon' => 'fa fa-star-o',
        ]) ?>
        <?php echo \insolita\wgadminlte\LteInfoBox::widget([
            'bgIconColor' => \insolita\wgadminlte\LteConst::COLOR_MAROON,
            'bgColor' => '',
            'number' => $created_courses_count,
            'text' => Yii::t('frontend/account/stats','Number of Created courses'),
            'icon' => 'fa fa-star',
        ]) ?>

        <?php
        if ($overall_score['max_score']) {
            echo \insolita\wgadminlte\LteInfoBox::widget([
                'bgIconColor' => '',
                'bgColor' => \insolita\wgadminlte\LteConst::COLOR_LIGHT_BLUE,
                'number' => $overall_score['gained_score'] . ' / ' . $overall_score['max_score'],
                'text' => Yii::t('frontend/account/stats', 'Overall score'),
                'icon' => 'fa fa-line-chart',
                'showProgress' => true,
                'progressNumber' => $overall_score['gained_score'] * 100 / $overall_score['max_score'],
                'description' => Yii::t('frontend/account/stats', 'Your score made of all completed tests'),
            ]);
        } ?>

    </div>
</div>
