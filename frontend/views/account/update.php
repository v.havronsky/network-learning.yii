<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('frontend/account', 'Edit my profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend/account', 'My profile'), 'url' => ['/account']];
$this->params['breadcrumbs'][] = Yii::t('frontend/account', 'Edit my profile');
?>
<div class="user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
