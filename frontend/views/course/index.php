<?php
/* @var $this yii\web\View */
/* @var $courses frontend\models\Course */
/* @var $searchModel frontend\models\CourseSearch */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;

$this->title = Yii::t('frontend/course', 'Course catalog');
?>
<h1><?= Yii::t('frontend/course', 'Course catalog') ?></h1>

<?php //Pjax::begin(); ?>
<div class="row">
    <div class="col-xs-12 col-md-9">


<?php foreach ($courses as $course): ?>

<!--without widget LteBox for now-->
<article class="box box-warning">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-3">
                <? //Yii::$app->formatter->asImage('images/courses/logo.png') ?>
                <?= Html::img('@web/images/courses/' . ($course->logo_img ? 'uploads/' . $course->logo_img : 'nopic.jpg'), ['alt' => 'course logo', 'class' => 'img-thumbnail']); ?> <!-- , 'style' => ['height' => '150px'] -->
            </div>
            <div class="col-sm-9">
                <div class="box-header with-border"><h3 class="box-title"><?= $course->name ?></h3></div>
                <div class="box-footer">
                    <div class="short-desc">
                        <?= $course->short_desc ?>
                    </div>
                    <div class="tools-block pull-right">
                        <?= Html::a('<i class="fa fa-eye"></i> ' . Yii::t('frontend/course', 'Overview course'), ['course/view', 'id' => $course->id], ['class' => 'btn btn-success btn-xs create_button p-2', 'data-pjax' => 0]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>

<?php endforeach; ?>

<?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>
<?//= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <section class="box box-purple">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('frontend/course', 'Filters')?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-pjax="0"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body p-0">
                <!--search-->
                <div class="box box-none">
                    <div class="box-header">
                        <h3 class="box-title"><?= Yii::t('frontend/course', 'Search') ?></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?= Yii::t('frontend/course', 'Search by name') ?>
                        <?php
                        $form = ActiveForm::begin([
//                            'id' => 'login-form',
                            'method' => 'get',
                            'action' => ['course/index'],
                            'options' => ['data-pjax' => 1],
                        ]) ?>
                        <?= $form->field($searchModel, 'name')->label(false) ?>
                        <?php ActiveForm::end() ?>
                    </div>
                </div>
                <!--sorting-->
                <div class="box box-none">
                    <div class="box-header">
                        <h3 class="box-title"><?= Yii::t('frontend/course', 'Sorting') ?></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?= Yii::t('frontend/course', 'Sort list by') ?>
                        <?= $dataProvider->getSort()->link('name') . ' | ' . $dataProvider->getSort()->link('creation_date'); ?>
                    </div>
                </div>
                <!--categories-->
<!--                <div class="box box-none">-->
<!--                    <div class="box-header">-->
<!--                        <h3 class="box-title">Categories</h3>-->
<!--                        <div class="box-tools pull-right">-->
<!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>-->
<!--                            </button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="box-body">-->
<!--                        cat content-->
<!--                        --><?////=  $form->field($model, 'items[]')->checkboxList(['a' => 'Item A', 'b' => 'Item B', 'c' => 'Item C']); ?>
<!--                    </div>-->
<!--                </div>-->

            </div>
        </section>
    </div>
</div>
<?php //Pjax::end(); ?>

