<?php
/* @var $this yii\web\View */
/* @var $courses frontend\models\Course */
/* @var $searchModel frontend\models\CourseSearch */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

$this->title = Yii::t('frontend/nav', 'My Courses');
?>

<h1><?= Yii::t('frontend/nav', 'My Courses') ?></h1>

<?php if (!empty($courses)): ?>
<div class="row">
    <div class="col-xs-12 col-md-9">

        <?php foreach ($courses as $course): ?>

        <?php  //var_dump($course->users2courses[0]->sign_date);die(__FILE__); ?>

            <!--without widget LteBox for now-->
            <article class="box box-warning">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <? //Yii::$app->formatter->asImage('images/courses/logo.png') ?>
                            <?= Html::img('@web/images/courses/' . ($course->logo_img ? 'uploads/' . $course->logo_img : 'nopic.jpg'), ['alt' => 'course logo', 'class' => 'img-thumbnail']); ?> <!-- , 'style' => ['height' => '150px'] -->
                        </div>
                        <div class="col-sm-9">
                            <div class="box-header with-border"><h3 class="box-title"><?= $course->name ?></h3></div>
                            <div class="box-footer">
                                <div class="short-desc">
                                    <?= $course->short_desc ?>
                                </div>
                                <div class="tools-block pull-right">
                                    <?= Html::a('<i class="fa fa-eye"></i> ' . Yii::t('frontend/course', 'Overview course'), ['course/view', 'id' => $course->id], ['class' => 'btn btn-success btn-xs create_button p-2', 'data-pjax' => 0]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

        <?php endforeach; ?>

        <?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>
        <?//= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <section class="box box-purple">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('frontend/course', 'Filters')?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-pjax="0"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body p-0">
                <!--search-->
                <div class="box box-none">
                    <div class="box-header">
                        <h3 class="box-title"><?= Yii::t('frontend/course', 'Search') ?></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?= Yii::t('frontend/course', 'Search by name') ?>
                        <?php
                        $form = ActiveForm::begin([
//                            'id' => 'login-form',
                            'method' => 'get',
                            'action' => ['course/index'],
                            'options' => ['data-pjax' => 1],
                        ]) ?>
                        <?= $form->field($searchModel, 'name')->label(false) ?>
                        <?php ActiveForm::end() ?>
                    </div>
                </div>
                <!--sorting-->
                <div class="box box-none">
                    <div class="box-header">
                        <h3 class="box-title"><?= Yii::t('frontend/course', 'Sorting') ?></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?= Yii::t('frontend/course', 'Sort list by') ?>
                        <?= $dataProvider->getSort()->link('name') . ' | ' . $dataProvider->getSort()->link('creation_date'); ?>
                    </div>
                </div>
                <!--categories-->
                <!--                <div class="box box-none">-->
                <!--                    <div class="box-header">-->
                <!--                        <h3 class="box-title">Categories</h3>-->
                <!--                        <div class="box-tools pull-right">-->
                <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>-->
                <!--                            </button>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                    <div class="box-body">-->
                <!--                        cat content-->
                <!--                        --><?////=  $form->field($model, 'items[]')->checkboxList(['a' => 'Item A', 'b' => 'Item B', 'c' => 'Item C']); ?>
                <!--                    </div>-->
                <!--                </div>-->

            </div>
        </section>
    </div>
</div>
<? endif; ?>

<?php if (empty($courses)): ?>
<div class="row">
    <div class="col-xs-12">
        <article class="box box-warning">
            <div class="box-body">
                <div class="box-footer">
                    <div style="min-height: 200px">
                        <h2><?= Yii::t('frontend/course/list', 'It seems you haven\'t subscribed to any courses yet...') ?></h2>
                        <p class="text-center">
                            <?= Html::a('<i class="fa fa-desktop"></i> ' . Yii::t('frontend/course/list', 'Browse catalog'), ['course/index'], ['class' => 'btn btn-success btn-xs create_button p-3 font-bigger']) ?>
                        </p>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
<? endif; ?>
