<?php
/* @var $this yii\web\View */
/* @var $course frontend\models\Course */
/* @var $long_desc_model common\models\CourseLongDesc */
/* @var $is_user_signed bool is user signed for course */

use yii\helpers\Html;
use yii\bootstrap\Tabs;

$this->title = $course->name;
$this->params['breadcrumbs'][] = $course->name;

?>

<!-- @TODO tab1content BLOCK -->
<?php $this->beginBlock('tab1content'); ?>
<div class="box-header with-border"><h3 class="box-title"><?= Yii::t('frontend/course', 'Course overview') ?></h3></div>
<div class="box-footer">
    <div class="long-desc">
        <? if ($long_desc_model): ?>
            <?= $long_desc_model->content ?>
        <? else: ?>
            <p>
                <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi asperiores, dolores esse, exercitationem iste laborum libero nesciunt nihil numquam omnis recusandae sequi similique voluptatibus? Adipisci aperiam iste numquam quas ut!</span><span>Doloremque eligendi expedita explicabo magni nesciunt pariatur provident quisquam temporibus. Expedita facere magnam maiores molestias nesciunt quam reprehenderit repudiandae, temporibus. Ea eum eveniet iste itaque laboriosam magni nam odio? Repudiandae!</span><span>At exercitationem ipsa molestias odit quidem repellendus sunt. Ex hic laborum provident quibusdam repellendus sint, tempora ullam. Commodi cum debitis, dolor expedita libero maiores nemo officiis porro provident quia totam.</span><span>Consequatur dolor minus mollitia omnis quo quod repudiandae sed. A asperiores laborum nam nobis, odio voluptatem? Assumenda delectus doloremque dolorum harum ipsa ipsum, odio quidem, quis sint tempora temporibus, unde.</span><span>Accusamus alias aut consectetur dicta esse eveniet excepturi harum in laborum maxime nemo nihil nisi officia perspiciatis quas quibusdam sapiente soluta ullam vero, voluptatum. Dolore error libero quidem repellat repudiandae?</span><span>Aliquam amet consectetur debitis deleniti doloremque doloribus ducimus eius excepturi id ipsa maxime, neque placeat, porro praesentium repellendus repudiandae voluptates? Dolorem est ex fugiat nobis officia, qui quod rem! Iusto.</span><span>Accusamus aliquid consectetur, deleniti deserunt dicta doloremque dolores ea esse eum eveniet fugiat incidunt iste, laboriosam maxime molestiae mollitia pariatur porro quae qui quibusdam repellat repellendus similique vel. Dolorum, neque!</span><span>Accusantium ad autem delectus, dicta dolor dolore earum enim eos error, exercitationem fugiat id illum in labore laboriosam magnam minima nobis odit perspiciatis quam quisquam quos sit unde vel voluptatum?</span><span>Corporis esse maiores officiis? Asperiores aspernatur cum dolorem ipsam nihil nisi odio? Aliquam eius fuga id iure libero magnam molestias nam obcaecati, praesentium qui quidem, repellat repellendus repudiandae unde veniam!</span><span>Accusantium aliquam amet animi atque deleniti deserunt eum explicabo facilis fugit illo, inventore ipsum labore laudantium libero maxime, nemo nesciunt nostrum numquam odio omnis optio quasi reprehenderit temporibus veniam veritatis.</span><span>Blanditiis distinctio expedita illo natus voluptate, voluptatum. Alias aliquam autem consequuntur cum distinctio ducimus error eveniet incidunt, necessitatibus sequi sint vel veniam voluptatibus! Accusantium adipisci earum, eos iste itaque maiores!</span><span>Ab expedita molestiae praesentium voluptas. Animi asperiores blanditiis consequuntur cumque, eaque ex impedit inventore laborum magnam maxime nisi odit omnis perferendis, quae ratione recusandae, rem repellat repellendus saepe soluta totam?</span><span>Ad adipisci, alias amet architecto aspernatur corporis debitis dignissimos dolore ea eligendi enim expedita hic itaque labore magni maiores minima molestiae nesciunt nostrum nulla perspiciatis quaerat quasi tempora vero vitae.</span><span>Animi asperiores autem cumque necessitatibus odit praesentium quidem vitae voluptatibus. Accusantium debitis facilis ipsa perferendis ut? Accusamus, delectus dignissimos? Aliquam eius eos explicabo laborum obcaecati. Facere inventore ipsam magnam provident?</span><span>Alias debitis ducimus ea facilis fuga id inventore iste iusto magnam minus nesciunt nihil non perspiciatis quo quod ratione rem rerum saepe sint soluta, totam ut veritatis vero voluptas voluptatum!</span><span>Atque dolorem earum eveniet fugit in magnam minima molestias perspiciatis, quaerat quo similique suscipit totam voluptatum. Deleniti ipsa magni necessitatibus nisi tempore! Adipisci asperiores blanditiis ipsam minus reprehenderit veniam voluptates?</span><span>Aliquam amet eaque id minima porro quibusdam tempore vitae? Dolorum ducimus eveniet, explicabo quidem reiciendis similique voluptatibus. Aliquid architecto consequuntur cum doloribus est, explicabo illo inventore, iure optio reiciendis totam?</span><span>At cupiditate ducimus exercitationem illum odio placeat quam quod repellat reprehenderit ullam? Eos illo nesciunt placeat possimus quas repellat sit temporibus! Aut blanditiis consequatur cumque dolore ducimus facilis maiores non.</span><span>Odit quibusdam rerum soluta. Architecto beatae blanditiis culpa dolore enim et eveniet explicabo illum laboriosam molestiae nam nesciunt non odit omnis quas recusandae rem, repudiandae saepe sunt totam. Adipisci, vel.</span><span>Aliquam architecto, consequuntur cupiditate dolorem ducimus eaque et ex expedita, fugiat ipsam itaque iusto laudantium modi praesentium quae quod reprehenderit, repudiandae rerum? Dolor doloremque eos illum in, libero quibusdam tenetur!</span><span>Amet, earum expedita fugit id iure necessitatibus quas quia. Aliquid aperiam consectetur corporis, cumque dignissimos eaque eveniet facilis harum ipsa laborum obcaecati quis reiciendis rem rerum suscipit unde voluptas, voluptates.</span><span>Accusamus aspernatur at doloribus ex explicabo in maiores nihil quod repellat repudiandae! Cupiditate earum enim fuga fugit id iusto libero quo soluta tempore tenetur? Assumenda consequuntur eum illo ratione recusandae.</span><span>Aliquam, animi beatae delectus dolores ducimus facere facilis fuga ipsam laborum magnam minus nulla pariatur perferendis quam quo, sapiente similique ullam velit. Amet atque blanditiis consequuntur fugiat natus, odio vel.</span><span>Consequuntur corporis deleniti, eligendi eum facere fuga harum minus nisi nobis non odio officiis recusandae rerum tenetur totam vel voluptates? Doloribus exercitationem explicabo iste necessitatibus nihil optio quia vitae. Veniam.</span><span>A adipisci alias asperiores aut autem consequatur cum debitis dicta est et facilis incidunt magni maxime minima molestiae nesciunt omnis optio quibusdam quidem quos ratione, rem sequi sint velit voluptas!</span><span>Adipisci culpa ex excepturi iusto natus neque odit placeat quam quidem temporibus! Alias aperiam cum deserunt dolorem dolores ex explicabo harum ipsa itaque laudantium magni maiores, maxime tenetur ullam velit!</span><span>Architecto dolore ducimus, ea fugit itaque iure, laborum maxime minima nesciunt placeat qui quibusdam ut voluptates. Accusantium assumenda consectetur cum illum odio qui repudiandae sint! Delectus magnam quas suscipit tenetur!</span><span>Ab autem consequuntur corporis debitis dolorem doloribus eaque error facere hic illum iure, iusto molestias omnis rem sequi temporibus vero! Cumque et harum inventore necessitatibus nisi praesentium quidem, sapiente voluptatibus!</span><span>Aliquam, dicta eos fugiat laboriosam, molestias mollitia obcaecati officiis quas ratione repellat, sequi veniam! Consequuntur dolorem dolores ducimus earum, harum modi nemo nostrum nulla perspiciatis quidem. Eius in quisquam ut?</span><span>Consectetur cumque dicta, doloribus fugiat itaque neque optio quas reiciendis suscipit veritatis. Accusantium alias culpa cupiditate dicta et explicabo id impedit iure, laudantium modi, necessitatibus nesciunt omnis porro quae totam.</span><span>Aliquam aliquid cumque deserunt doloribus enim ex excepturi itaque natus nulla numquam, omnis pariatur soluta suscipit tempore temporibus totam, voluptate voluptatem. Atque consequuntur distinctio dolor exercitationem placeat sequi temporibus totam.</span><span>Architecto illum incidunt itaque iure iusto labore laudantium mollitia nesciunt non, quos sed tempora vero voluptate? A beatae consequatur consequuntur deleniti, deserunt, eaque eius facilis impedit mollitia natus nisi provident.</span><span>Consequuntur cum dignissimos dolores eos, laudantium saepe sequi velit voluptas voluptates. Alias animi commodi corporis cupiditate esse facilis hic ipsa ipsum minus odit placeat possimus quidem recusandae sint, unde, voluptatum!</span><span>Aliquid atque debitis ex harum incidunt iste laborum, nihil optio provident quia quod ratione rem sit soluta totam unde voluptatem. Beatae delectus dolore est fuga modi quas ratione tempora vel.</span><span>Adipisci amet aperiam at atque aut commodi consequatur doloribus ducimus ea eos, est explicabo harum ipsa laboriosam natus neque officiis, placeat quisquam reiciendis saepe temporibus tenetur vitae? Maiores, nemo quisquam.</span><span>Ab accusamus aliquid architecto asperiores at blanditiis culpa deleniti dignissimos eveniet ex illo, illum libero, neque non numquam quia reiciendis rerum sed tenetur ut. Accusantium beatae impedit laudantium quia tempore.</span><span>A ad aliquid at dicta distinctio dolorem eos eum facilis illo incidunt, iste iure, laudantium, maiores minus necessitatibus provident quidem quis quisquam quod sed suscipit tempore temporibus veritatis voluptas voluptates?</span><span>Aliquam architecto assumenda consequuntur, deserunt dolore doloribus ducimus expedita fugit incidunt ipsum itaque laborum libero natus nesciunt, non odio officiis saepe suscipit, temporibus voluptatibus! Accusantium atque esse et eum fugiat?</span><span>Ab accusamus amet aperiam at dignissimos, doloremque, eaque eius eligendi excepturi exercitationem incidunt labore laborum nostrum numquam obcaecati officia officiis pariatur porro quas quibusdam, quidem repellat repellendus saepe velit voluptates.</span><span>A aliquam amet atque aut deserunt dolorum ea fuga fugit iusto molestiae mollitia odio officia optio pariatur quae, quaerat quas quidem ratione rem repellat repudiandae sequi soluta tempore. Omnis, unde.</span><span>Ad adipisci aliquid amet animi aspernatur consectetur distinctio fugiat id itaque labore laboriosam, laudantium nobis obcaecati, quae quo saepe sequi, vel voluptate. Ducimus facilis maxime nesciunt quod soluta velit vero.</span><span>Aperiam aspernatur consectetur corporis cumque dolore doloremque dolores, ducimus ea, error et explicabo id in ipsum libero molestias nisi placeat possimus quisquam repellat reprehenderit repudiandae sed ullam veniam voluptas voluptates.</span><span>Ab asperiores assumenda cum dolor dolore eius est fugiat, id illo modi nisi qui quidem, ratione, repellat rerum tenetur vero? Accusamus delectus excepturi temporibus veniam. Dolor est impedit maxime optio.</span><span>Accusantium architecto consequuntur distinctio dolores, exercitationem facilis fugit incidunt ipsa ipsam, itaque magnam magni minima molestias obcaecati officia quas ratione recusandae sed sit voluptatibus. Ad dolores eaque laborum provident temporibus?</span><span>Alias atque consequuntur corporis cupiditate deleniti, dolore dolorem doloremque doloribus est in ipsam ipsum iure libero maiores minima nam nesciunt non officia quae qui ratione recusandae repudiandae totam velit voluptate.</span><span>Animi aspernatur assumenda cum deserunt, dignissimos doloribus eligendi illum incidunt itaque iure laudantium libero molestiae nihil omnis optio placeat possimus quaerat quam quos rem sapiente temporibus tenetur vero voluptatibus voluptatum.</span><span>Ab cupiditate ea eaque libero natus, optio quasi sit voluptatem! Aliquam blanditiis esse illo labore perferendis quasi qui sunt tempore! Eligendi enim facilis laboriosam libero nemo nulla numquam, voluptatum! Odio.</span><span>Adipisci consequatur cupiditate error et, ex explicabo fuga fugiat inventore itaque laboriosam libero mollitia, nam numquam, praesentium quae quia ullam voluptas voluptatem. At, doloremque unde. Consectetur dolorum reprehenderit sapiente sint.</span><span>Accusantium aperiam consequuntur, cupiditate ducimus eaque maiores minima obcaecati recusandae ullam voluptatum? Aliquam aliquid deleniti deserunt esse excepturi facere impedit, iure maxime modi, natus officiis porro possimus quasi rem reprehenderit?</span><span>At beatae corporis, delectus ea earum ipsum, iure laudantium magnam, nulla officia porro sapiente sit voluptatum. Culpa, ex facere illum ipsa, ipsum magnam obcaecati officiis quas quia sed soluta, tenetur.</span>
            </p>
        <? endif; ?>
    </div>
    <div class="tools-block pull-right">
        <?php
        if (!$is_user_signed) {
            echo Html::a(
                '<i class="fa fa-sign-in"></i> ' . Yii::t('frontend/course', 'Subscribe to course'),
                ['course/sign-up', 'id' => $course->id],
                ['data-method' => 'post', 'class' => 'btn btn-success btn-xs create_button p-2'],
                );
        } ?>
    </div>
</div>
<?php $this->endBlock(); ?>

<h1><?= $course->name ?></h1>

<div class="row">
    <div class="col-xs-12 col-md-9">

        <article class="box box-warning">
            <div class="box-body">

                <div class="nav-tabs-custom">
                    <?php
                    $items = [
                        [
                            'label' => Yii::t('frontend/course', 'Main page'),
                            'content' => $this->blocks['tab1content'],
                            'active' => true
                        ],
                        [
                            'label' => Yii::t('frontend/course', 'Course'),
                            'content' => $course_lessons_list,
//                                'headerOptions' => [],
//                                'options' => ['id' => 'myveryownID'],
                        ],
                    ];

                    if ($course->test) {
                        $items[] = [
//                            'label' => Yii::t('frontend/course', 'Course'),
//                            'content' => $course_lessons_list,
                            'label' => '<i class="fa fa-edit"></i>  <span>' . $course->test->name . '</span>',
                            'url' => ['/test', 'id' => $course->test->id],
                        ];

//                        var_dump($items);die(__FILE__);
                    }

                    echo Tabs::widget([
                        'encodeLabels' => false,
                        'items' => $items,
                    ]);
                    ?>
                </div>
            </div>
        </article>

    </div>
    <div class="col-xs-12 col-md-3">
        <section class="box box-purple fixed-block">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('frontend/course', 'Additional information') ?></h3>
            </div>
            <div class="box-body">
                <p><?= Yii::t('frontend/course', 'Number of lessons: <b>{0}</b>', count($course->lessons)) ?></p>
                <p><?= Yii::t('frontend/course','Course rating:') ?> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></p>
                <p><?= Yii::t('frontend/course', 'Number of students:') ?> <span style="font-weight: bold">10 000+</span></p>
                <p></p>
                <p><?= Yii::t('frontend/course', 'Last update:') ?> <b><?= Yii::$app->formatter->asDate($course->creation_date, 'long'); ?></b></p>
                <p>
                    <?php
                    if (!$is_user_signed) {
                        echo Html::a(
                            '<i class="fa fa-sign-in"></i> ' . Yii::t('frontend/course', 'Subscribe to course'),
                            ['course/sign-up', 'id' => $course->id],
                            ['data-method' => 'post', 'class' => 'btn btn-success btn-xs create_button p-2']);
                    } ?>
                </p>
            </div>
        </section>
    </div>
</div>