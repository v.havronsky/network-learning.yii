<?php
/* @var $this yii\web\View */
/* @var $lessons common\models\Lesson */

use yii\bootstrap\Nav;
?>

<?php
$items = [];
foreach ($lessons as $lesson) {
    $items[] = [
        'label' => $lesson->name,
        'url' => ['lesson/', 'id' => $lesson->id]
    ];
}

if ($items) {
    echo Nav::widget([
        'items' => $items,
        'options' => ['class' => 'nav-stacked'], // set this to nav-tab to get tab-styled navigation
    ]);
} else {
    echo "<h3>" . Yii::t('frontend/course', 'No lessons found!') . "</h3>";
}

?>

<!--<ul class="nav nav-stacked">-->
<!--    <li><a href="#">Projects <span class="pull-right badge bg-blue">31</span></a></li>-->
<!--    <li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>-->
<!--    <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>-->
<!--    <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>-->
<!--</ul>-->

<?// if ($lessons): ?>
<!--<ul>-->
<!--    --><?// foreach ($lessons as $lesson): ?>
<!---->
<!--    <li>--><?//= $lesson->name ?><!--</li>-->
<!--    ----->
<!--    --><?// endforeach; ?>
<!--</ul>-->
<!---->
<?// else: ?>
<!--    <h2>No lessons found!</h2>-->
<?// endif; ?>
