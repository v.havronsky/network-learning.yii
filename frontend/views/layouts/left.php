<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?php if (!Yii::$app->user->isGuest): ?>
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= Html::img('@web/images/users/' . (Yii::$app->user->identity->avatar ? 'avatars/' . Yii::$app->user->identity->avatar : 'user_logo.jpg'), ['alt' => 'User Image', 'class' => 'img-circle']); ?>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity ? Yii::$app->user->identity->username : 'unknown' ?></p>
                <a href="<?= Url::toRoute('/account') ?>"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?php endif; ?>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="<?= Yii::t('frontend/nav', 'Search...') ?>"/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' =>  Yii::t('frontend/nav', 'Main menu'), 'options' => ['class' => 'header']],

                    ['label' => Yii::t('frontend/nav', 'My Courses'), 'icon' => 'star-o', 'url' => ['/course/list'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => Yii::t('frontend/nav', 'Own Courses'), 'icon' => 'star', 'url' => ['/account-course'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => Yii::t('frontend/nav', 'Courses'), 'icon' => 'list', 'url' => ['/course']],

//                    ['label' => 'Stats / Progress', 'icon' => 'line-chart', 'url' => ['/gii'], 'visible' => !Yii::$app->user->isGuest],
                    // @todo add separator? <hr>
                    ['label' => Yii::t('frontend/nav', 'Sign in'), 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    ['label' => Yii::t('frontend/nav', 'Register'), 'url' => ['site/signup'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
