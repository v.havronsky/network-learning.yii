<?php
/* @var $this yii\web\View */
/* @var $lesson common\models\Lesson */
/* @var $lesson_steps common\models\LessonStep */

//use yii\bootstrap\Nav;
use yii\bootstrap\Tabs;

$this->title = $lesson->name;
$this->params['breadcrumbs'][] = ['label' => $lesson->course->name, 'url' => ['/course/view', 'id' => $lesson->course->id]];
$this->params['breadcrumbs'][] = $lesson->name;

?>


<h1><?= $lesson->name ?></h1>

<div class="row">
    <div class="col-xs-12 col-md-12">

        <article class="box box-warning">
            <div class="box-body">

                <?php
                $items = [];
                foreach ($lesson_steps as $step) {
                    $items[] = [
                        'label' => '<i class="fa ' . $step->icon . '"></i>  <span>' . $step->name . '</span>', //@todo change
//                        'url' => ['lesson/', 'id' => $lesson->id],
                        'content' => $step->content,
                    ];
                }

                // test tab if available
                if ($lesson->test) {
                    $items[] = [
                        'label' => '<i class="fa fa-edit"></i>  <span>' . $lesson->test->name . '</span>',
                        'url' => ['/test', 'id' => $lesson->test->id],
                    ];
                }

                if ($items) {
                    echo Tabs::widget([
                        'encodeLabels' => false,
                        'items' => $items,
                    ]);
                } else {
                    echo "<h3>" . Yii::t('frontend/course', 'No STEPS found!') . "</h3>";
                }

                ?>
            </div>
        </article>

    </div>
</div>

