<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('common/login', 'Registration');

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$appNamePrefixLogo = '@web/images/logo-molecule.png';
?>

<div class="login-box">
    <div class="login-logo">
        <?= Html::a('<span class="logo-lg">' . Html::img($appNamePrefixLogo, ['alt' => 'logo', 'width' => '40']) . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('common/login', 'Please fill out the following fields to signup:') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username'), ['autofocus' => true]]) ?>

        <?= $form
            ->field($model, 'email', $fieldOptions3)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-12">
                <?= Html::submitButton(Yii::t('common/login', 'Join now!'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'signup-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <br>
        <?= Yii::t('common/login', 'Already have an account?') ?>
        <?= Html::a( Yii::t('common/login', 'Sign in!'), ['site/login']) ?>
    </div>
</div>
