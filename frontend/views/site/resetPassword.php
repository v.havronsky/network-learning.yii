<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = Yii::t('common/login', 'Reset password');

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$appNamePrefixLogo = '@web/images/logo-molecule.png';
?>

<div class="login-box">
    <div class="login-logo">
        <?= Html::a('<span class="logo-lg">' . Html::img($appNamePrefixLogo, ['alt' => 'logo', 'width' => '40']) . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('common/login', 'Please choose your new password:') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autofocus' => true]) ?>

        <div class="row">
            <div class="col-xs-12">
                <?= Html::submitButton(Yii::t('common/login', 'Save'), ['class' => 'btn btn-primary btn-block btn-flat']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
