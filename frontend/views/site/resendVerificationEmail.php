<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = Yii::t('common/login', 'Resend verification email');

$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$appNamePrefixLogo = '@web/images/logo-molecule.png';
?>

<div class="login-box">
    <div class="login-logo">
        <?= Html::a('<span class="logo-lg">' . Html::img($appNamePrefixLogo, ['alt' => 'logo', 'width' => '40']) . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('common/login', 'Please fill out your email. A verification email will be sent there.') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'resend-verification-email-form']); ?>

        <?= $form
            ->field($model, 'email', $fieldOptions3)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'autofocus' => true]) ?>

        <div class="row">
            <div class="col-xs-12">
                <?= Html::submitButton(Yii::t('common/login', 'Send'), ['class' => 'btn btn-primary btn-block btn-flat']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
