<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('common/login', 'Sign In');

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$appNamePrefixLogo = '@web/images/logo-molecule.png';
?>

<div class="login-box">
    <div class="login-logo">
        <?= Html::a('<span class="logo-lg">' . Html::img($appNamePrefixLogo, ['alt' => 'logo', 'width' => '40']) . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('common/login', 'Sign in to start your session') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <div class="col-xs-6">
                <?= Html::submitButton(Yii::t('common/login', 'Sign in'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <div class="social-auth-links text-center">
            <p>- <?= Yii::t('common/login', 'OR') ?> -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> <?= Yii::t('common/login', 'Sign in using Facebook') ?></a>
            <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> <?= Yii::t('common/login', 'Sign in using Google+') ?></a>
        </div>

        <?= Html::a( Yii::t('common/login', 'I forgot my password'), ['site/request-password-reset']) ?>
        <br>
        <?= Html::a( Yii::t('common/login', 'Register a new account'), ['site/signup']) ?>
        <br>
        <?= Yii::t('common/login', 'Need new verification email?') ?>
        <?= Html::a( Yii::t('common/login', 'Resend'), ['site/resend-verification-email']) ?>
    </div>
</div>
