<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\course\Course */
/* @var $long_desc_model common\models\CourseLongDesc */

$this->title = 'Update Course: ' . $model->name;
$this->title = Yii::t('backend/course', 'Update {modelClass}: ', [
    'modelClass' => $model->name
]);

$this->params['breadcrumbs'][] =  ['label' => Yii::t('frontend/account/course', 'Own courses'), 'url' => ['/account-course']];
$this->params['breadcrumbs'][] = ['label' => $model->name];

?>
<div class="course-update">

    <?= $this->render('_form', [
        'model' => $model,
        'long_desc_model' => $long_desc_model,
    ]) ?>

</div>
