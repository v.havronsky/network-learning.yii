<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\course\Course */
/* @var $long_desc_model common\models\CourseLongDesc */

$this->title = Yii::t('backend/course', 'Create Course');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/course', 'Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-create">

    <?= $this->render('_form', [
        'model' => $model,
        'long_desc_model' => $long_desc_model,
    ]) ?>

</div>
