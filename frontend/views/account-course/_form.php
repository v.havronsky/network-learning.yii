<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\course\Course */
/* @var $long_desc_model common\models\CourseLongDesc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form box box-warning">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'id_user')->hiddenInput(['value'=> Yii::$app->user->id])->label(false) ?>

        <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

        <!--  @todo relation data content -->
        <?= $form->field($long_desc_model, 'content')->widget(CKEditor::className(),[
        'editorOptions' => [
                'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ],
        ]); ?>


        <?= $form->field($model, 'active')->hiddenInput(['value'=> 0])->label(false) ?>

        <?= $form->field($model, 'upload')->fileInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('backend/course', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
