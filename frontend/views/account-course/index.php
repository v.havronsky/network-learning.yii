<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $courses frontend\models\Course */
/* @var $searchModel frontend\models\CourseSearch */

$this->title = Yii::t('frontend/account/course', 'Own courses | {0}', $model->username);
$this->params['breadcrumbs'][] = Yii::t('frontend/account/course', 'Own courses');
?>

<?php if (!empty($courses)): ?>
<div class="header-box pb-3">
    <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('frontend/account/course', 'Create new course'), ['account-course/create'], ['class' => 'btn btn-success btn-xs create_button p-3 font-bigger']) ?>
</div>
<? endif; ?>

<div class="row">
    <div class="col-xs-12">

        <?php foreach ($courses as $course): ?>

            <article class="box box-warning">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <? //Yii::$app->formatter->asImage('images/courses/logo.png') ?>
                            <?= Html::img('@web/images/courses/' . ($course->logo_img ? 'uploads/' . $course->logo_img : 'nopic.jpg'), ['alt' => 'course logo', 'class' => 'img-thumbnail']); ?> <!-- , 'style' => ['height' => '150px'] -->
                        </div>
                        <div class="col-sm-9">
                            <div class="box-header with-border"><h3 class="box-title"><?= $course->name ?></h3></div>
                            <div class="box-footer">
                                <div class="short-desc">
                                    <?= $course->short_desc ?>
                                </div>
                                <div class="pull-left">
<!--                                    --><?//= Html::a('<i class="fa fa-eye"></i> Overview course', ['course/view', 'id' => $course->id], ['class' => 'btn btn-success btn-xs create_button p-2', 'data-pjax' => 0]) ?>
                                    <?php if ($course->active): ?>
                                        <span class="text-success">
                                            <b><?= Yii::t('frontend/account/course', 'Is active') ?></b>
                                        </span>
                                    <?php else: ?>
                                        <span class="text-danger">
                                            <b><?= Yii::t('frontend/account/course', 'On moderation') ?></b>
                                        </span>
                                    <?endif;?>
                                </div>
                                <div class="tools-block pull-right">
                                    <?= Html::a('<i class="fa fa-eye"></i> ' . Yii::t('frontend/account/course', 'Edit course'), ['account-course/update', 'id' => $course->id], ['class' => 'btn btn-warning btn-xs create_button p-2']) ?>
                                    <?php if ($course->active): ?>
                                        <?= Html::a('<i class="fa fa-eye"></i> ' . Yii::t('frontend/course', 'Overview course'), ['course/view', 'id' => $course->id], ['class' => 'btn btn-success btn-xs create_button p-2']) ?>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

        <?php endforeach; ?>

        <?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>

        <?php if (empty($courses)): ?>
            <article class="box box-warning">
                <div class="box-body">
                    <div class="box-footer">
                        <div style="min-height: 650px">
                            <h2><?= Yii::t('frontend/account/course', 'It seems you haven\'t created any courses yet...') ?></h2>
                            <p class="text-center">
                                <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('frontend/account/course', 'Create now!'), ['account-course/create'], ['class' => 'btn btn-success btn-xs create_button p-3 font-bigger']) ?>
                            </p>
                        </div>
                    </div>
                </div>
            </article>
        <? endif; ?>
    </div>
</div>
