<?php

namespace frontend\models;

use common\models\User2Test;
use Yii;
use common\models\Test;
use yii\db\Query;

class TestForm extends Test
{
    protected $test;

    public $userAnswers = [];
    public $userWrongAnswers = [];

    public $questionsSet = [];
    public $correctAnswers = [];
    public $totalMark;

    public $otherUsersAvgMark;

    public function rules()
    {
        return [
            ['userAnswers', 'required', 'message' => Yii::t('frontend/test', 'You have to give an answer for this.')]
        ];
    }

    public function getTestForm($test)
    {
        $this->test = $test;

        foreach ($test->questions as $question) {
            $correctAnswers = [];
            $answersSet = [];
            foreach ($question->answers as $answer) {
                $answersSet[$answer->id] = $answer->text;

                if ($answer->is_correct) {
                    $correctAnswers[] = $answer->id;
                }
            }

            if (count($correctAnswers) == 1) {
                $correctAnswers = $correctAnswers[0]; //@todo kostyl
            }

            $this->questionsSet[$question->id] = [
                'label' => $question->text,
                'type' => $this->getAnswersType($correctAnswers),
                'answers' => $answersSet,
            ];
            $this->correctAnswers[$question->id] = $correctAnswers;
            $this->totalMark += $question->cost;
        }
    }

    protected function getTest()
    {
        return $this->test;
    }

    protected function getAnswersType($answers)
    {
        return is_array($answers) ? 'checkboxList' : 'radioList';
    }

    public function checkAnswers()
    {
        if (empty($this->userAnswers)) {
            return false;
        }

        $userMark = 0;
        $userCorrectAnswersCount = 0;
        foreach ($this->getTest()->questions as $question) {
            if ($this->userAnswers[$question->id] == $this->correctAnswers[$question->id]) {
                $userMark += $question->cost;
                $userCorrectAnswersCount++;
            } else {
                $this->userWrongAnswers[] = $question->id;
            }
        }

        // get test object
        $test = $this->getTest();

        // save results to user2test object
        $user2test = new User2Test();

        $user2test->id_user = Yii::$app->user->id;
        $user2test->id_test = $test->id;
//        $user2test->complete_date = time(); // made by mysql
        $user2test->mark = $userMark;
        $user2test->max_mark = $this->totalMark;
        $user2test->questions_count = count($test->questions);
        $user2test->questions_correct_count = $userCorrectAnswersCount;

        if ($user2test->save(false)) {
            // my results compared to other students
            $other_users_avg_mark = (new Query())
                ->from('user2test')
                ->where('id_test=:id_test AND id_user!=:id_user')
                ->addParams([
                    ':id_user' => Yii::$app->user->id,
                    ':id_test' => $test->id,
                ])
                ->average('mark');

            $this->otherUsersAvgMark = $other_users_avg_mark;

            // reload model
            $user2test = User2Test::findOne($user2test->id);

            // set course to completed
            if ($test->id_course && $user2course = User2Course::findOne(['id_user' => Yii::$app->user->id,'id_course' => $test->id_course])) {
                $user2course->complete_date = $user2test->complete_date;
                $user2course->save();
            }

            return $user2test;
        }

        return false;
    }
}
