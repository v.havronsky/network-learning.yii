<?php

namespace frontend\models;

use common\models\Lesson;
use common\models\Test;
use common\models\User;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $name
 * @property int $id_user
 * @property string $short_desc
 * @property int $active
 * @property string $logo_img
 * @property string $creation_date
 * @property string $timestamp
 */
class Course extends \common\models\Course
{
    public function getUsers2courses()
    {
        return $this->hasMany(User2Course::class, ['id_course' => 'id']);
    }
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'id_user'])->via('users2courses');
    }

    public function getLessons()
    {
        return $this->hasMany(Lesson::class, ['id_course' => 'id'])->orderBy(['pos' => SORT_ASC]);
    }

    public function getTest()
    {
        return $this->hasOne(Test::class, ['id_course' => 'id']);
    }

    public static function assignUser($id_user, $id_course)
    {
        $user2Course = new User2Course();
        $user2Course->id_user = $id_user;
        $user2Course->id_course = $id_course;
        return $user2Course->save();
    }
}
