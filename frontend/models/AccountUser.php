<?php

namespace frontend\models;

use Yii;
use common\models\User;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $role
 * @property string $birthday
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $verification_token
 */
class AccountUser extends User
{
    /**
     * @var UploadedFile Image to be uploaded
     */
    public $upload;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['birthday'], 'safe'],
            [['birthday'], 'date', 'format' => 'php:Y-m-d'],
            [['username', 'email', 'first_name', 'last_name'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['upload'], 'file', 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'avatar' => Yii::t('app', 'Avatar'),
            'birthday' => Yii::t('app', 'Birthday'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}