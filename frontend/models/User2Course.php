<?php

namespace frontend\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "user2course".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_course
 * @property string $sign_date
 * @property string $complete_date
 */
class User2Course extends \common\models\User2Course
{
    public function _()
    {
        return $this::findAll([
            'active' => 1,
        ]);
    }
}
