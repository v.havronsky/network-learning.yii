<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Course;

/**
 * CourseSearch represents the model behind the search form of `frontend\models\Course`.
 */
class CourseSearch extends Course
{
    // @todo used for attributes, but useless right now
//    public $user;
//    public $user2course;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'active'], 'integer'],
            [['name', 'short_desc', 'logo_img', 'creation_date', 'timestamp'], 'safe'],
//            [['user', 'users', 'user2course'], 'safe'], //@todo dunno if it is right
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $query->andFilterWhere(['active' => 1]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'active' => $this->active,
            'creation_date' => $this->creation_date,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'logo_img', $this->logo_img]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $user_id
     *
     * @return ActiveDataProvider
     */
    public function searchMy($params, $user_id)
    {
        $query = Course::find();

        // add conditions that should always apply here
        if ($user_id) {
            $query->joinWith(['users']);
            $query->andFilterWhere(['user.id' => $user_id]);
            $query->andFilterWhere(['active' => 1]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        // @todo IT WORKS! for sorting!
        $dataProvider->sort->attributes['sign_date'] = [
            'asc' => ['user2course.sign_date' => SORT_ASC],
            'desc' => ['user2course.sign_date' => SORT_DESC],
        ];
        // @todo default sorting
        $dataProvider->sort->defaultOrder = ['sign_date' => SORT_DESC];
//        $dataProvider->setSort([
////            'defaultOrder' => ['sign_date'=>SORT_DESC],
//        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'active' => $this->active,
            'creation_date' => $this->creation_date,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'logo_img', $this->logo_img]);

//var_dump($query->createCommand()->sql);die(__FILE__);
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $user_id
     *
     * @return ActiveDataProvider
     */
    public function searchOwn($params, $user_id)
    {
        $query = Course::find();

//        // add conditions that should always apply here
        if ($user_id) {
//            $query->joinWith(['users']);
            $query->andFilterWhere(['id_user' => $user_id]);
//            $query->andFilterWhere(['active' => 1]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
//
//        $dataProvider->sort->attributes['sign_date'] = [
//            'asc' => ['user2course.sign_date' => SORT_ASC],
//            'desc' => ['user2course.sign_date' => SORT_DESC],
//        ];

        $dataProvider->sort->defaultOrder = ['creation_date' => SORT_DESC];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'active' => $this->active,
            'creation_date' => $this->creation_date,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'logo_img', $this->logo_img]);

        return $dataProvider;
    }
}
