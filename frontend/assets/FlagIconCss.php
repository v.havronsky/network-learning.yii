<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class FlagIconCss extends AssetBundle
{
    public $sourcePath = '@bower/flag-icon-css';

    public $css = [
        'css/flag-icon.min.css',
    ];
}
