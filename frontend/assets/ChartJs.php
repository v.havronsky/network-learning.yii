<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class ChartJs extends AssetBundle
{
    public $sourcePath = '@bower/chart-js';

    public $js = [
        'dist/Chart.min.js',
    ];
}
