<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TestQuestionAnswer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/test', 'Test Question Answers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-question-answer-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('backend/test', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('backend/test', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('backend/test', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'id_question',
                'text:ntext',
                'pos',
                'is_correct',
                'timestamp',
            ],
        ]) ?>
    </div>
</div>
