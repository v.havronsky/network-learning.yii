<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TestQuestionAnswer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-question-answer-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'id_question')->textInput() ?>

<!--        --><?//= $form->field($model, 'text')->widget(CKEditor::class,[
//            'editorOptions' => [
//                'preset' => 'basic',
//                'inline' => false,
//            ],
//        ]); ?>
        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'pos')->textInput() ?>

        <?= $form->field($model, 'is_correct')->checkbox() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('backend/test', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
