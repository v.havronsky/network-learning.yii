<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TestQuestionAnswer */

$this->title = Yii::t('backend/test', 'Update {modelClass}: ', [
    'modelClass' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/test', 'Test Question Answers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/test', 'Update');
?>
<div class="test-question-answer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
