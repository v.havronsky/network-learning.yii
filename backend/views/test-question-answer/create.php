<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TestQuestionAnswer */

$this->title = Yii::t('backend/test', 'Create Test Question Answer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/test', 'Test Question Answers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-question-answer-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
