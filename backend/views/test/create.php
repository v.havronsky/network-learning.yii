<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Test */

$this->title = Yii::t('backend/test', 'Create Test');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/test', 'Tests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
