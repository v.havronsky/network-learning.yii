<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Test */

$this->title = Yii::t('backend/test', 'Update {modelClass}: ', [
    'modelClass' => 'Test',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/test', 'Tests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/test', 'Update');
?>
<div class="test-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
