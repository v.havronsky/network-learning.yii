<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TestQuestion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/test', 'Test Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-question-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('backend/test', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('backend/test', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('backend/test', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a(Yii::t('backend/test', 'To the Test Question Answers'), ['/test-question-answer', 'TestQuestionAnswerSearch[id_question]' => $model->id], ['class'=>'btn btn-success pull-right']) ?>
        <?= Html::a(Yii::t('backend/test', 'Create Debug Test Question Answers'), ['/test-question/create-debug', 'id' => $model->id], ['class'=>'btn btn-success pull-right', 'style' => 'margin-right:10px']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'id_test',
                'text:ntext',
                'pos',
                'cost',
                'timestamp',
            ],
        ]) ?>
    </div>
</div>
