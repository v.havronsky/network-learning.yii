<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TestQuestion */

$this->title = Yii::t('backend/test', 'Update {modelClass}: ', [
    'modelClass' => 'Test Question',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/test', 'Test Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/test', 'Update');
?>
<div class="test-question-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
