<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LessonStep */

$this->title = Yii::t('backend/lesson', 'Create Lesson Step');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/lesson', 'Lesson Steps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-step-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
