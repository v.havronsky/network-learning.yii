<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\LessonStep */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/lesson', 'Lesson Steps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-step-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('backend/lesson', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('backend/lesson', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('backend/lesson', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'id_lesson',
                'name',
                'pos',
                'active',
                'content:ntext',
                'icon',
                'creation_date',
                'timestamp',
            ],
        ]) ?>
    </div>
</div>
