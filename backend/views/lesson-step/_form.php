<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LessonStep */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lesson-step-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'id_lesson')->textInput() ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'pos')->textInput() ?>

        <?//= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'content')->widget(CKEditor::className(),[
            'editorOptions' => [
                'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ],
        ]); ?>

        <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>

        <?//= $form->field($model, 'creation_date')->textInput() ?>

        <?//= $form->field($model, 'timestamp')->textInput() ?>

        <?= $form->field($model, 'active')->checkbox() ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('backend/lesson', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
