<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Lesson */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/lesson', 'Lessons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('backend/lesson', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('backend/lesson', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('backend/lesson', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('backend/lesson', 'To the Lesson Steps'), ['/lesson-step', 'LessonStepSearch[id_lesson]' => $model->id], ['class'=>'btn btn-success pull-right']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'id_course',
                'name',
                'pos',
                'active',
                'short_desc:ntext',
                'creation_date',
                'data:ntext',
                'timestamp',
            ],
        ]) ?>
    </div>
</div>
