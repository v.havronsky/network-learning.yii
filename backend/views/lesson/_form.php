<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Lesson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lesson-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'id_course')->textInput() ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'pos')->textInput() ?>

        <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

        <?//= $form->field($model, 'creation_date')->textInput() ?>

        <?//= $form->field($model, 'data')->textarea(['rows' => 6]) ?>

        <?//= $form->field($model, 'timestamp')->textInput() ?>

        <?= $form->field($model, 'active')->checkbox() ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('backend/lesson', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
