<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LessonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend/lesson', 'Lessons');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a(Yii::t('backend/lesson', 'Create Lesson'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'id_course',
                'name',
                'pos',
                'active',
                // 'short_desc:ntext',
                // 'creation_date',
                // 'data:ntext',
                // 'timestamp',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
