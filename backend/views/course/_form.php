<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\course\Course */
/* @var $long_desc_model common\models\CourseLongDesc */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(); ?>

<div class="course-form box box-primary">
    <!--  RANDOM button for debugging  -->
<!--    <div class="box-header with-border">-->
<!--        <h3 class="box-title">--><?//= Html::a('Randomize!', Url::current(['random' => true]), ['class'=>'btn btn-primary']) ?><!--</h3>-->
<!--    </div>-->
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'id_user')->textInput() ?>

        <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

        <!--  @todo relation data content -->
        <?= $form->field($long_desc_model, 'content')->widget(CKEditor::className(),[
        'editorOptions' => [
                'preset' => 'full',
                'inline' => false,
            ],
        ]); ?>

        <?= $form->field($model, 'active')->checkbox() ?>

        <?= $form->field($model, 'upload')->fileInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('backend/course', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php Pjax::end(); ?>
