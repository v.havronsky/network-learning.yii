<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\course\Course */
/* @var $long_desc_model common\models\CourseLongDesc */

$this->title = 'Update Course: ' . $model->name;
$this->title = Yii::t('backend/course', 'Update {modelClass}: ', [
        'modelClass' => 'Course',
    ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="course-update">

    <?= $this->render('_form', [
        'model' => $model,
        'long_desc_model' => $long_desc_model,
    ]) ?>

</div>
