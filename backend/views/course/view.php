<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\course\Course */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/course','Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('backend/course','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('backend/course','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('backend/course','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('backend/course', 'To the Lessons'), ['/lesson', 'LessonSearch[id_course]' => $model->id], ['class'=>'btn btn-success pull-right']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'id_user',
                'short_desc:ntext',
                'courseLongDesc.content', //@todo relation data content
                'active',
                'logo_img',
                'creation_date',
                'timestamp',
            ],
        ]) ?>
    </div>
</div>
