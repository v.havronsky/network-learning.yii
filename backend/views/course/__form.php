<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\course\Course */
/* @var $form yii\widgets\ActiveForm */

//var_dump(Url::current(['random' => true]), Url::to(['random' => true], true) , Url::to(['random' => true]));die(__FILE__);
?>

<?php Pjax::begin(); ?>

<?= Html::a('Randomize!', Url::current(['random' => true]), ['class'=>'btn btn-primary']) ?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'logo_img')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php Pjax::end(); ?>
