<?php

namespace backend\models\course;

use Yii;
//use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $name
 * @property int $id_user
 * @property string $short_desc
 * @property int $active
 * @property string $logo_img
 * @property string $creation_date
 * @property string $timestamp
 */
class Course extends \common\models\Course
{
    /**
     * @var UploadedFile Image to be uploaded
     */
    public $upload;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id_user', 'active'], 'integer'],
            [['short_desc'], 'string'],
            [['active'], 'boolean'],
//            [['creation_date', 'timestamp'], 'safe'],
//            [['name', 'logo_img'], 'string', 'max' => 255],
//            [['logo_img'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['upload'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'name' => 'Name',
//            'id_user' => 'Parent ID',
//            'short_desc' => 'Short Desc',
//            'active' => 'Active',
//            'logo_img' => 'Logo Img',
//            'creation_date' => 'Creation Date',
//            'timestamp' => 'Timestamp',
//        ];
//    }
}
