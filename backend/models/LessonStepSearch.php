<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LessonStep;

/**
 * LessonStepSearch represents the model behind the search form of `common\models\LessonStep`.
 */
class LessonStepSearch extends LessonStep
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_lesson', 'pos', 'active'], 'integer'],
            [['name', 'content', 'icon', 'creation_date', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LessonStep::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_lesson' => $this->id_lesson,
            'pos' => $this->pos,
            'active' => $this->active,
            'creation_date' => $this->creation_date,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'icon', $this->icon]);

        return $dataProvider;
    }
}
