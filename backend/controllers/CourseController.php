<?php

namespace backend\controllers;

use common\models\CourseLongDesc;
use Yii;
use backend\models\course\Course;
use backend\models\course\CourseSearch;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Faker;
use common\models\UploadForm;
use yii\web\UploadedFile;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param bool $random
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();
        $long_desc_model = new CourseLongDesc();

        // @todo
        if ($this->handlePostSave($model)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'long_desc_model' => $long_desc_model,
        ]);
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param bool $random
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!$long_desc_model = $model->courseLongDesc) {
            $long_desc_model = new CourseLongDesc();
        }

        if ($this->handlePostSave($model)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'long_desc_model' => $long_desc_model,
        ]);
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        //@todo delete long desc
        try {
            if ($this->findModel($id)->delete()) {
                if ($rel = CourseLongDesc::find()->where(['id_course' => $id])->one()) {
                    $rel->delete();
                }
            }
        } catch (NotFoundHttpException $e) {
            Yii::$app->session->setFlash('error', "{$e->getName()}: {$e->getMessage()}");
        } catch (\Throwable $e) {
            Yii::$app->session->setFlash('error', "{$e->getName()}: {$e->getMessage()}");
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error', "{$e->getName()}: {$e->getMessage()}");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function handlePostSave(Course $model)
    {
        if ($model->load(Yii::$app->request->post())) {
            $model->upload = UploadedFile::getInstance($model, 'upload');

            if ($model->validate()) {
                // image handler
                if ($model->upload) {
                    $fileName = $model->upload->baseName . '.' . $model->upload->extension;
                    $filePath = Yii::getAlias('@webroot-common/images/courses/uploads/' . $fileName);

                    if ($model->upload->saveAs($filePath)) {
                        $model->logo_img = $fileName;
                    }
                }

                if ($model->save(false)) {
                    // long desc handler
                    if (!$long_desc_model = $model->courseLongDesc) {
                        $long_desc_model = new CourseLongDesc();
                    }
                    $data['CourseLongDesc'] = Yii::$app->request->post()['CourseLongDesc'];
                    $data['CourseLongDesc']['id_course'] = $model->id;
                    if ($long_desc_model->load($data) && $long_desc_model->save()) {
                        return true;
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }
        }

        return false;
    }
}
