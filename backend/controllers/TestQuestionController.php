<?php

namespace backend\controllers;

use Yii;
use common\models\TestQuestion;
use backend\models\TestQuestionSearch;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TestQuestionController implements the CRUD actions for TestQuestion model.
 */
class TestQuestionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TestQuestion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestQuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TestQuestion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TestQuestion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TestQuestion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TestQuestion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TestQuestion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCreateDebug($id)
    {
        $model = $this->findModel($id);

        $connection = Yii::$app->db;
        $connection->createCommand()->insert('test_question_answers', [
            'id_question' => $id,
            'text' => 'А) Варіант відповіді',
            'pos' => 10,
            'is_correct' => 1,
        ])->execute();
        $connection->createCommand()->insert('test_question_answers', [
            'id_question' => $id,
            'text' => 'Б) Варіант відповіді',
            'pos' => 20,
            'is_correct' => 0,
        ])->execute();
        $connection->createCommand()->insert('test_question_answers', [
            'id_question' => $id,
            'text' => 'В) Варіант відповіді',
            'pos' => 20,
            'is_correct' => 0,
        ])->execute();
        $connection->createCommand()->insert('test_question_answers', [
            'id_question' => $id,
            'text' => 'Г) Варіант відповіді',
            'pos' => 20,
            'is_correct' => 0,
        ])->execute();

        return $this->redirect(['/test-question-answer', 'TestQuestionAnswerSearch[id_question]' => $model->id]);
    }

    /**
     * Finds the TestQuestion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TestQuestion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TestQuestion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
